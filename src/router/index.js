import Vue from 'vue'
import Router from 'vue-router'
import Step from '../components/formStep/Step'
import Simulation from '../components/simulation/Simulation.vue'
// import FormStep from '@/components/FormStep'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Step',
      component: Step
    },
    {
      path: '/simulator',
      name: 'Simulation',
      component: Simulation
    }
  ]
})
